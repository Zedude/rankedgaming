import React from 'react'

export const Pagination = (props) => {
         const { amongUs, krunker, minecraft, pubg, className } = props

         return (
                  <ul className={`pagination ${className}`}>
                           <li className={`waves-effect ${amongUs}`}><a href="/classes?game=amongUs">Among Us</a></li>
                           <li className={`waves-effect ${krunker}`}><a href="/classes?game=krunker">Krunker</a></li>
                           <li className={`waves-effect ${minecraft}`}><a href="/classes?game=minecraft">Minecraft</a></li>
                           <li className={`waves-effect ${pubg}`}><a href="/classes?game=pubg">PUBG</a></li>
                  </ul>
         )
}