import React from 'react';

export const Input = (props) => {
    let { className, type, ...others } = props;
    return (
        <>
            {type === 'password' &&
                <input className={`input br-primary-1 c-primary ${className}`} type='password' {...others} />}
            {type === 'email' &&
                <input className={`input br-primary-1 c-primary ${className}`} type='email' {...others} />}
            {type !== 'password' && type !== 'email' &&
                <input className={`input br-primary-1 c-primary ${className}`} {...others} />}
        </>
    );
};