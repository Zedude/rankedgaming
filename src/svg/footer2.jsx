import React from 'react'

export const Footer2 = () => {
         return (<>
                  <svg width="1024" height="53" viewBox="0 0 1024 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <path d="M6 53H0V0L1024 0V6L6 6V53Z" fill="#38F84B" />
                  </svg>
                  <div className='footer2Text c-primary'>
                           RoundiMation’s youtube channel
                           Zedude’s other creations
                  </div>
         </>)
}