import React from 'react'

export const Message = (props) => {
         const { type, children } = props

         return (<>
                  {
                           type === 'white' &&
                           <div className='flex'>
                                    <div className='message message--white c-primary b-black'>{children}</div>
                           </div>
                  }
                  {
                           type === 'blue' &&
                           <div className='justify-end flex'>
                                    <div className='message message--blue c-black b-primary justify-end'>{children}</div>
                           </div>
                  }
         </>)
}
