import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Button, MobileLayout, useDoc } from '../components';
import { useFirebase } from '../firebase';
import { AupContext } from '../providers/aup';

export const AccountM = () => {
         const history = useHistory()
         const { auth } = useFirebase();
         const { user } = useContext(AupContext)
         const [text, setText] = useState('users/nothing')
         useEffect(() => {
                  if (user) {
                           setText(`users/${user.email}`)
                  }
         }, [user])
         const data = useDoc(text)
         console.log(data)
         const signOut = () => {
                  auth.signOut();
                  history.push('/')
         }

         return (
                  <MobileLayout>
                           {(user) &&
                                    <>
                                             <div className='containerAccountM flex-center'>
                                                      {data.data.rank !== 'Guest' &&
                                                               <img width='150px' height='150px' src={data.data.rankUrl} alt='ZedudeIsCool' />}
                                                      <div className='c-primary fs-2-5'>{data.data.firstName} {data.data.lastName}</div>
                                                      <div className='c-primary'>Email: {user.email}</div>
                                                      <Button className='c-black mt-2-4' onClick={signOut}>Log Out</Button>
                                             </div>
                                    </>}
                  </MobileLayout>
         )
}