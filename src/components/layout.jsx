import accountCircleOutline from '@iconify/icons-mdi/account-circle-outline';
import controllerClassicOutline from '@iconify/icons-mdi/controller-classic-outline';
import googleClassroom from '@iconify/icons-mdi/google-classroom';
import homeOutline from '@iconify/icons-mdi/home-outline';
import messageOutline from '@iconify/icons-mdi/message-outline';
import shieldStarOutline from '@iconify/icons-mdi/shield-star-outline';
import { Icon } from '@iconify/react';
import React from 'react';
import { slide as Menu } from 'react-burger-menu';

export const Layout = ({ children, ...others }) => {
    return (<div className='hide-on-med-and-down'>
        <Menu pageWrapId='page-wrap' className='hide-on-med-and-down'>
            <a className="menu-item flex justify-start items-center" href="/">
                <Icon icon={homeOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                <div className='ml-2-5'>Home</div>
            </a>
            <a className="menu-item flex justify-start items-center" href="/chat">
                <Icon icon={messageOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                <div className='ml-2-5'>Chat</div>
            </a>
            <a className="menu-item flex justify-start items-center" href="/account">
                <Icon icon={accountCircleOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                <div className='ml-2-5'>Account</div>
            </a>
            <a className="menu-item flex justify-start items-center" href="/ranks">
                <Icon icon={shieldStarOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                <div className='ml-2-5'>Ranks</div>
            </a>
            <a className="menu-item flex justify-start items-center" href="/games">
                <Icon icon={controllerClassicOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                <div className='ml-2-5'>Games</div>
            </a>
        </Menu>
        <div className={'flex b-black flex-col items-center pa-3 vhw'}{...others}>
            <div className='w100 flex-1 b-black br-1-1 flex-center'>
                {children}
            </div>

            <div className='black w100 flex justify-between hide-on-med-and-down h-2-5'>
                <div className='c-primary footer1Text hide-on-med-and-down'>Lisenced by RoundiMations
Server hosted by Zedude</div>
                <div className='c-primary footer2Text hide-on-med-and-down'>RoundiMation’s youtube channel
Zedude’s other creations</div>
                <div className='c-primary footer3Text hide-on-med-and-down'>Copyright © 2020 RANKED GAMES v.2020.10a. All rights reserved</div>
            </div>
        </div>
    </div>);
};