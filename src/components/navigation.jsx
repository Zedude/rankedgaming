import React from 'react';
import arrowLeft from '@iconify/icons-mdi/arrow-left'
import { useHistory, useLocation } from 'react-router-dom'
import { useContext } from 'react';
import { AupContext } from '../providers/aup';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDoc } from './useCollection';
import { Icon, InlineIcon } from '@iconify/react';

export const Navigation = () => {
    const history = useHistory();
    const location = useLocation();
    const { user } = useContext(AupContext)
    const [usr, setUsr] = useState(null)
    const [text, setText] = useState('users/nothing')
    useEffect(() => {
        if (user) {
            setText(`users/${user.email}`)
        }
    }, [user])
    const data = useDoc(text)
    useEffect(() => {
        if (data) {
            setUsr(data.data.name)
            console.log(data)
        }
    }, [data])

    const goToHome = () => {
        history.push('/');
    }
    const goToLogin = () => {
        history.push('/login')
    }
    return (
        <div className='w100 flex justify-end items-center align-self-start black'>
            <Icon icon={arrowLeft} style={{color: '#38f84b', fontSize: '50px'}}></Icon>
        </div>
    );
};