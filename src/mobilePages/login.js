import React, { useContext, useState } from 'react';
import { Layout, Button, Input, Link, useKeyPress, MobileLayout } from '../components';
import { useHistory } from 'react-router-dom';
import { AupContext } from '../providers/aup'
import { useFirebase } from '../firebase';

export const LoginM = () => {
         const history = useHistory();
         const { auth } = useFirebase();
         const { user } = useContext(AupContext);
         const [email, setEmail] = useState('')
         const [password, setPassword] = useState('')
         const [error, setError] = useState('')

         const goToSignUp = () => {
                  history.push('/signup')
         }

         const signIn = async () => {
                  if (!email || !password) {
                           setError('Please enter the required fields')
                           return;
                  }
                  if (password.toLowerCase() === 'password') {
                           setError('Password is not a good password')
                           return;
                  }

                  try {
                           await auth.signInWithEmailAndPassword(email, password);
                           history.push('/')
                  } catch (e) {
                           setError(e.message);
                  }
         }

         if (user) {
                  history.push('/')
         }

         if (useKeyPress(13)) {
                  signIn()
         }

         return (<>
                  <MobileLayout>
                           <div className='signUpPage c-default flex-center'>
                                    <Input className='w500M'type='email' placeholder='Email' value={email} onChange={(e) => { setEmail(e.target.value) }}></Input>
                                    <Input className='w500M' type='password' placeholder='Password' value={password} onChange={(e) => { setPassword(e.target.value) }}></Input>
                                    <Button className='signUpButton c-black w450M' onClick={signIn}>Login</Button>
                                    <div className='c-red'>{error}</div>
                           </div>
                  </MobileLayout>
         </>)
}