import React from 'react'

export const Footer1 = () => {
         return (<div className='footer1Container'>
                  <svg width="203" height="53" viewBox="0 0 203 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <path d="M197 53H203V0H0V6H197V53Z" fill="#38F84B" />
                  </svg>
                  <div className='c-primary footer1Text'>Lisenced by RoundiMations 
Server hosted by Zedude</div>
         </div>)
}