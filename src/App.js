import React from 'react';
import {
    BrowserRouter as Router,

    Route, Switch
} from "react-router-dom";
import { AccountM, ChatM, LoginM, RanksExplanationM, SignUpM } from './mobilePages';
import { GamesM } from './mobilePages/games';
import { HomeDefaultM } from './mobilePages/home-default';
import { Account, Chat, Games, HomeDefault, Login, NotFound, RanksExplanation, SignUp } from './pages';
import { AupProvider } from './providers/aup';
import './style/main.scss';

const App = () => {
    return (
        <AupProvider>
            <Router>
                <Switch>
                    <Route path="/" exact>
                        <HomeDefault />
                        <HomeDefaultM />
                    </Route>
                    <Route path="/login">
                        <Login />
                        <LoginM />
                    </Route>
                    <Route path="/signup">
                        <SignUp />
                        <SignUpM />
                    </Route>
                    <Route path="/account">
                        <Account />
                        <AccountM />
                    </Route>
                    <Route path="/ranks">
                        <RanksExplanation />
                        <RanksExplanationM />
                    </Route>
                    <Route path='/games'>
                        <Games />
                        <GamesM />
                    </Route>
                    <Route path="/chat">
                        <Chat />
                        <ChatM />
                    </Route>
                    <Route>
                        <NotFound />
                    </Route>
                </Switch>
            </Router>
        </AupProvider>
    )
}

export default App;