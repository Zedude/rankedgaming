import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { useFirebase } from '../firebase'
import { useCollection, useDoc } from '../components'

const TimeLine = () => {
         const { firestore } = useFirebase()
         const [info, setInfo] = useState(null)
         const [active, setActive] = useState(false)
         const { data, updateRecord } = useCollection(`schedule`)
         const [scheduleS, setScheduleS] = useState([])
         const [url, setUrl] = useState('')
         const [nextGame, setNextGame] = useState()

         const scheduleFunc = () => {
                  let cart = []

                  data.forEach((datas) => {
                           const dat = datas
                           console.log(dat)
                           if (!nextGame) {
                                    setNextGame({
                                             url: dat.gameUrl,
                                             name: dat.game
                                    })
                           }
                           const date = new Date(dat.startTime.seconds * 1000)
                           let week = date.getDay()
                           switch (week) {
                                    case 0: week = 'Mon'; break
                                    case 1: week = 'Tue'; break
                                    case 2: week = 'Wed'; break
                                    case 3: week = 'Thu'; break
                                    case 4: week = 'Fri'; break
                                    case 5: week = 'Sat'; break
                                    case 6: week = 'Sun'; break
                           }
                           const day = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
                           const time = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2)
                           const dateE = new Date(dat.endTime.seconds * 1000)
                           const dayE = dateE.getFullYear() + '-' + (dateE.getMonth() + 1) + '-' + dateE.getDate()
                           const timeE = ('0' + dateE.getHours()).slice(-2) + ':' + ('0' + dateE.getMinutes()).slice(-2) + ':' + ('0' + dateE.getSeconds()).slice(-2)

                           if (day in cart) {
                                    cart[day].push({
                                             week,
                                             game: dat.game,
                                             startTime: dat.startTime,
                                             endTime: dat.endTime,
                                             startedAt: day + ' ' + time,
                                             endAt: dayE + ' ' + timeE
                                    })
                           } else {
                                    cart[day] = [{
                                             week,
                                             game: dat.game,
                                             startTime: dat.startTime,
                                             endTime: dat.endTime,
                                             startedAt: day + ' ' + time,
                                             endAt: dayE + ' ' + timeE
                                    }]
                           }
                  })

                  console.log(cart)

                  setScheduleS(cart)
         }

         useEffect(() => {
                  if (data[0]) {
                           scheduleFunc(data)
                  }
         }, [data])

         return (
                  <>
                           <div className="scheduleContainer hide-on-med-and-down">
                                    <div className='timeLINEGetIt1'></div>
                                    {
                                             Object.keys(scheduleS).sort().map((day, index) => (
                                                      <>
                                                               <div key={index} className={"flex-row flex justify-between mrgn-16"}>
                                                                        <div className='w-5 items-center flex flex-col'>
                                                                                 <div className={'scheduleDate'}>
                                                                                          {day.toString().split('-')[2]}
                                                                                 </div>
                                                                                 <div className='font-caption'>
                                                                                          {scheduleS[day][0].week}
                                                                                 </div>
                                                                        </div>
                                                                        <div className={'timeLineCircle'}></div>
                                                                        <div>
                                                                                 {scheduleS[day].sort().map((schedule, i) => {
                                                                                          return (
                                                                                                   <div className='c-primary flex-center fs-3-2'>
                                                                                                            {schedule.game}
                                                                                                            <div className={'scheduleButton fs-2-4'}>
                                                                                                                     {schedule.startedAt.toString().split(' ')[1].split(':')[0] + ':' + schedule.startedAt.toString().split(' ')[1].split(':')[1]}---
                                                                                                                     {schedule.endAt.toString().split(' ')[1].split(':')[0] + ':' + schedule.endAt.toString().split(' ')[1].split(':')[1]}
                                                                                                            </div>
                                                                                                   </div>
                                                                                          )
                                                                                 })}
                                                                        </div>
                                                               </div>
                                                      </>
                                             ))
                                    }
                           </div>
                           {nextGame &&
                                    <>
                                             <img className='imageGame hide-on-med-and-down' src={nextGame.url} />
                                    </>}
                           <div className="scheduleContainerM hide-on-large-only">
                                    <div className='timeLINEGetItM'></div>
                                    {
                                             Object.keys(scheduleS).sort().map((day, index) => (
                                                      <>
                                                               <div key={index} className={"flex-row flex justify-between mrgn-16M "}>
                                                                        <div className='w-5 items-center flex flex-col'>
                                                                                 <div className={'scheduleDateM'}>
                                                                                          {day.toString().split('-')[2]}
                                                                                 </div>
                                                                                 <div className='font-caption'>
                                                                                          {scheduleS[day][0].week}
                                                                                 </div>
                                                                        </div>
                                                                        <div className={'timeLineCircleM'}></div>
                                                                        <div>
                                                                                 {scheduleS[day].sort().map((schedule, i) => {
                                                                                          return (
                                                                                                   <div className='c-primary flex-center fs-3-2'>
                                                                                                            {schedule.game}
                                                                                                            <div className={'scheduleButton fs-2-4'}>
                                                                                                                     {schedule.startedAt.toString().split(' ')[1].split(':')[0] + ':' + schedule.startedAt.toString().split(' ')[1].split(':')[1]}---
                                                                                                                     {schedule.endAt.toString().split(' ')[1].split(':')[0] + ':' + schedule.endAt.toString().split(' ')[1].split(':')[1]}
                                                                                                            </div>
                                                                                                   </div>
                                                                                          )
                                                                                 })}
                                                                        </div>
                                                               </div>
                                                      </>
                                             ))
                                    }
                           </div>
                  </>
         )
}

export { TimeLine }