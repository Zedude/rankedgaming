import sendOutline from '@iconify/icons-mdi/send-outline'
import Icon from '@iconify/react'
import React, { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import ScrollToBottom from 'react-scroll-to-bottom'
import uniqid from 'uniqid'
import { Input, MobileLayout } from '../components/'
import { Message } from '../components/message'
import { useDoc, useRTDB } from '../components/useCollection'
import { useFirebase } from '../firebase'
import { AupContext } from '../providers/aup'

export const ChatM = () => {
         const history = useHistory()
         const { user } = useContext(AupContext)
         const [count, setCount] = useState(0)
         const { rtdb } = useFirebase();
         const { firestore } = useFirebase()
         const [list, setList] = useState([])
         const [loading, setLoading] = useState(false)
         const [hasMore, setHasMore] = useState(true)
         const [cursor, setCursor] = useState((new Date()).getTime())
         const { addData, state } = useRTDB('id11');
         const [text, setText] = useState('')
         const now = new Date();
         const [newMessages, setNewMessages] = useState([])
         const [email, setEmail] = useState('lol')
         useEffect(() => {
                  if (user) {
                           setEmail(user.email)
                  }
         }, [user])
         const { data } = useDoc(`users/${email}`)
         const [object, setObject] = useState({
                  firstSent: true,
                  bottomReached: false
         })

         const handleScroll = (event) => {
                  const target = event.target;

                  if (target.scrollTop < 10) {
                           loadMore()
                  }
         }

         const loadMoreMessage = async (index) => {
                  let lastRef = rtdb.ref('id11').orderByChild("createdAt").endAt(cursor).limitToLast(20)
                  let bottom = object.bottomReached;

                  await lastRef.once("value", async function (snapshot) {
                           if (snapshot.val() === null) {
                                    setHasMore(false)
                                    return;
                           }
                           const data = snapshot.val();
                           const keys = Object.keys(data);

                           setCursor(data[keys[0]].createdAt - 1)

                           const loadedMessages = keys.map(key => data[key])
                           const custom = {
                                    type: 'custom',
                                    index: index
                           }

                           setList(prevData => [
                                    ...loadedMessages,
                                    custom,
                                    ...prevData
                           ]);
                           if (bottom) {
                                    document.getElementById('messagesEnd').scrollIntoView()
                           }
                  })
         }

         const loadMore = async () => {
                  let firstLoad = object.firstSent

                  if (loading === true || hasMore === false) {
                           return;
                  }

                  setLoading(true)
                  const i = uniqid('midGame')

                  await loadMoreMessage(i)

                  setLoading(false)

                  if (firstLoad === true) {
                           document.getElementById(i).scrollIntoView()
                           setObject((object) => ({ firstSent: false, bottomReached: object.bottomReached }))
                  } else if (hasMore && document.getElementById(i)) {
                           document.getElementById(i).scrollIntoView()
                  }
         }

         const sendMessage = async () => {
                  if (text !== '' && user) {
                           const id = uniqid('message-')
                           let d = new Date();
                           await addData(id, {
                                    message: text,
                                    createdAt: d.getTime(),
                                    sentFrom: data.firstName
                           })
                           setText('')
                           if (document.getElementById('messagesEnd')) {
                                    document.getElementById('messagesEnd').scrollIntoView({ behavior: "smooth" })
                           }
                  }
         }

         const renderItem = (item) => {
                  return (<>
                           {item.type === 'custom' &&
                                    <div id={item.index}>
                                    </div>
                           }
                           {
                                    item.sentFrom !== data.firstName && !item.type &&
                                    <div className='c-primary'>
                                             {item.sentFrom}
                                             <Message type='white' id={`message-${item.createdAt}`} key={`message-${item.createdAt}`}>
                                                      <span>
                                                               {item.message}
                                                      </span>
                                             </Message>
                                    </div>
                           }
                           {
                                    item.sentFrom === data.firstName &&
                                    <div>
                                             <Message type='blue' id={`message-${item.createdAt}`} key={`message-${item.createdAt}`}>
                                                      <span>
                                                               {item.message}
                                                      </span>
                                             </Message>
                                    </div>
                           }
                  </>)
         }
         const doNothing = () => {

         }

         const trackScrolling = () => {
                  const wrappedElement = document.getElementById('messages');
                  if (wrappedElement === null) {
                           return
                  }
                  if (wrappedElement.scrollHeight - wrappedElement.scrollTop === wrappedElement.clientHeight) {
                           document.removeEventListener('scroll', this);
                           setObject((object) => ({ firstSent: object.firstSent, bottomReached: true }))
                  } else {
                           setObject((object) => ({ firstSent: object.firstSent, bottomReached: false }))
                  }
         }

         const LoadFirst = async () => {
                  if (rtdb) {
                           document.getElementById('messages').addEventListener('scroll', trackScrolling)
                           const dataRead = rtdb.ref('id11').orderByChild("createdAt").startAt(now.getTime());
                           await dataRead.on('value', function (snapshot) {
                                    const data = snapshot.val();
                                    let keys
                                    if (data) {
                                             keys = Object.keys(data);
                                             const loadedMessages = keys.map(key => data[key])
                                             setNewMessages(loadedMessages)
                                    }
                           });
                  }

                  if (rtdb) {
                           await loadMore()
                  }
         }

         useEffect(() => {
                  LoadFirst()
         }, [rtdb]);

         return (<>
                  <MobileLayout>
                           <div className='flex-column h100 chatContainerM flex justify-center hide-on-med-and-down'>
                                    <div
                                             onScroll={handleScroll}
                                             id='messages'
                                             className='LazyLoadM'
                                    >
                                             {user &&
                                                      <>
                                                               {
                                                                        loading &&
                                                                        <div> loading ... </div>
                                                               } {
                                                                        list && list.map(renderItem)
                                                               } {
                                                                        newMessages && newMessages.map(renderItem)
                                                               }
                                                      </>}
                                             <div id='messagesEnd'></div>
                                    </div>
                                    <div className='w100-221M flex-center flex-row '>
                                             <Input className='chatInputM' placeholder='Enter text here' value={text} onKeyPress={(e) => e.key === 'Enter' ? sendMessage() : doNothing()} onChange={(e) => setText(e.target.value)}></Input>
                                             <div onClick={sendMessage} className='pointer'>
                                                      <Icon icon={sendOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                                             </div>
                                    </div>
                           </div>
                  </MobileLayout>
         </>)
}