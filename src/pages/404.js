import React from 'react'
import { Layout } from '../components'

export const NotFound = () => {
         return (
                  <div style={{ height: '100vh' }} className='w100 flex-center black'>
                           <div className='loginPage c-default flex-center br-primary-3 brr pa-2-5'>
                                    <div className='fs-2-5 c-primary'>404 not found. :(</div>
                                    <div className='mt-2-5 flex-center c-primary'>
                                             <p>
                                                      Sorry, this page can't be found
                                             </p>
                                    </div>
                           </div>
                  </div>
         )
}