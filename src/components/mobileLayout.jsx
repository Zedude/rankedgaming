import accountCircleOutline from '@iconify/icons-mdi/account-circle-outline';
import controllerClassicOutline from '@iconify/icons-mdi/controller-classic-outline';
import googleClassroom from '@iconify/icons-mdi/google-classroom';
import homeOutline from '@iconify/icons-mdi/home-outline';
import messageOutline from '@iconify/icons-mdi/message-outline';
import shieldStarOutline from '@iconify/icons-mdi/shield-star-outline';
import { Icon } from '@iconify/react';
import React from 'react';
import { slide as Menu } from 'react-burger-menu';

export const MobileLayout = ({ children, ...others }) => {
         return (<div className='hide-on-large-only'>
                  <Menu pageWrapId='page-wrapM' className='hide-on-large-only'>
                           <a className="menu-item flex justify-start items-center" href="/">
                                    <Icon icon={homeOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                                    <div className='ml-2-5'>Home</div>
                           </a>
                           <a className="menu-item flex justify-start items-center" href="/chat">
                                    <Icon icon={messageOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                                    <div className='ml-2-5'>Chat</div>
                           </a>
                           <a className="menu-item flex justify-start items-center" href="/account">
                                    <Icon icon={accountCircleOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                                    <div className='ml-2-5'>Account</div>
                           </a>
                           <a className="menu-item flex justify-start items-center" href="/ranks">
                                    <Icon icon={shieldStarOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                                    <div className='ml-2-5'>Ranks</div>
                           </a>
                           <a className="menu-item flex justify-start items-center" href="/games">
                                    <Icon icon={controllerClassicOutline} style={{ color: '#38f84b', fontSize: '40px' }} />
                                    <div className='ml-2-5'>Games</div>
                           </a>
                  </Menu>
                  <div className={'flex flex-col b-black items-center pa-3 b-black vhw hide-on-large-only'}{...others}>
                           <div className='w100 flex-1 b-black flex-center hide-on-large-only'>
                                    {children}
                           </div>
                  </div>
         </div>);
};