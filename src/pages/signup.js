import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Input, Layout, useCollection, useKeyPress } from '../components';
import { useFirebase } from '../firebase';
import { AupContext } from '../providers/aup';

export const SignUp = () => {
    const { createRecord } = useCollection('users')
    const history = useHistory();
    const { auth } = useFirebase();
    const { user } = useContext(AupContext);
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [password2, setPassword2] = useState('')
    const [firstName, setFirstName] = useState('')
    const [name, setName] = useState('')
    const [error, setError] = useState('')

    const signUp = async () => {
        if (!email || !password || !password2) {
            setError('Please enter the required fields')
            return;
        }
        if (password2 !== password) {
            setError('Passwords do not match')
            return;
        }
        if (password.toLowerCase() === 'password') {
            setError('Password is not a good password')
            return;
        }
        try {
            await auth.createUserWithEmailAndPassword(email, password);
            createRecord(email, {
                lastName: name,
                firstName: firstName,
                rank: 'Guest'
            })
            history.push('/')
        } catch (e) {
            setError(e.message);
        }
    }

    if (user) {
        history.push('/')
    }

    if (useKeyPress(13)) {
        signUp()
    }

    return (
        <Layout>
            <div className='signUpPage c-default flex-center'>
                <Input type='email' className='w500' placeholder='E-mail' value={email} onChange={(e) => {setEmail(e.target.value)}}></Input>
                <div className='flex-row flex items-between w450'>
                    <Input className='' placeholder='First Name' value={firstName} onChange={(e) => {setFirstName(e.target.value)}}></Input>
                    <Input placeholder='Last Name' value={name} onChange={(e) => {setName(e.target.value)}}></Input>
                </div>
                <Input type='password' className='w500' placeholder='Password' value={password} onChange={(e) => {setPassword(e.target.value)}}></Input>
                <Input type='password' className='w500' placeholder='Confirm Password' value={password2} onChange={(e) => {setPassword2(e.target.value)}}></Input>
                <Button className='signUpButton c-black' onClick={signUp}>Sign Up</Button>
                <a href='' onClick={() => {history.push('/login')}}>Already have an account?</a>
                <div className='c-red'>{error}</div>
            </div>
        </Layout>
    )
}