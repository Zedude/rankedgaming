import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { MobileLayout, useCollection } from '../components';

export const GamesM = () => {
         const { data } = useCollection('games')
         console.log(data)

         return (
                  <MobileLayout>
                           <div className='w100-221M carouselContainM'>
                                    <Carousel className='carousel87 flex-center'>
                                             {data.map((item, i) => {
                                                      return (
                                                               <div className="card black">
                                                                        <div className="card-image activator">
                                                                                 <img className="activator" src={item.img} />
                                                                        </div>
                                                                        <div className="card-content">
                                                                                 <span className="card-title activator c-primary">{item.name}</span>
                                                                        </div>
                                                                        <div className="card-reveal black c-primary">
                                                                                 <span className="card-title c-primary black">{item.name}</span>
                                                                                 <p>{item.desc}</p>
                                                                        </div>
                                                               </div>
                                                      )
                                             })}
                                    </Carousel>
                           </div>
                  </MobileLayout>
         )
}