import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Layout, useCollection, useDoc } from '../components';
import { TimeLine } from '../components/timeline';
import { AupContext } from '../providers/aup';

export const HomeDefault = () => {
         const { user } = useContext(AupContext)
         const history = useHistory()
         const { createRecord } = useCollection('users')
         const [text, setText] = useState('users/nothing')
         useEffect(() => {
                  if (user) {
                           setText(`users/${user.email}`)
                  }
         }, [user])
         const data = useDoc(text)

         const thing = async () => {
                  if (data) {
                           await createRecord(user.email, {
                                    firstName: data.data.firstName,
                                    lastName: data.data.lastName,
                                    rank: 'Student'
                           })
                           setTimeout(() => {
                                    history.go(0)
                           }, 1000)
                  }
         }

         return (<>
                  <Layout>
                           <div className='mt-2-5 mb-2-5 flex-center'>
                                    {(user) &&
                                             <>
                                                      <TimeLine />
                                             </>}
                                    {(!user) &&
                                             <Button className='b-primary c-black' onClick={() => { history.push('/signup') }}>CREATE AN ACCOUNT</Button>
                                    }
                           </div>
                  </Layout>
         </>)
}