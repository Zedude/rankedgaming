import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import { MobileLayout, useCollection } from '../components';
import "react-responsive-carousel/lib/styles/carousel.min.css";

export const RanksExplanationM = () => {
         const { data } = useCollection('ranks')

         console.log(data)
         const sorter = (a, b) => {
                  let thing = 0;
                  if (a.game > b.game) {
                           thing++;
                  } else {
                           thing--;
                  }
                  return thing;
         }

         return (
                  <MobileLayout>
                           <div className='w100-221M carouselContainM'>
                                    <Carousel className='carousel87 flex-center'>
                                             {data.sort(sorter).map((item, i) => {
                                                      return (
                                                               <div className="card black">
                                                                        <div className="card-image activator">
                                                                                 <img className="activator" src={item.url} />
                                                                        </div>
                                                                        <div className="card-content">
                                                                                 <span className="card-title activator c-primary">{item.name}</span>
                                                                        </div>
                                                                        <div className="card-reveal black c-primary">
                                                                                 <span className="card-title c-primary black">{item.name}</span>
                                                                                 <p>{item.desc}</p>
                                                                        </div>
                                                               </div>
                                                      )
                                             })}
                                    </Carousel>
                           </div>
                  </MobileLayout>
         )
}