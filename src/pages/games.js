import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import { Layout, useCollection } from '../components';
import "react-responsive-carousel/lib/styles/carousel.min.css";

export const Games = () => {
         const { data } = useCollection('games')
         console.log(data)

         return (
                  <Layout>
                           <div className='timeLINEGetIt1'></div>
                           <div className='w100-221 carouselContain'>
                                    <Carousel className='carousel87 flex-center'>
                                             {data.map((item, i) => {
                                                      return (
                                                               <div className="card black">
                                                                        <div className="card-image activator">
                                                                                 <img className="activator" src={item.img} />
                                                                        </div>
                                                                        <div className="card-content">
                                                                                 <span className="card-title activator c-primary">{item.name}</span>
                                                                        </div>
                                                                        <div className="card-reveal black c-primary">
                                                                                 <span className="card-title c-primary black">{item.name}</span>
                                                                                 <p>{item.desc}</p>
                                                                        </div>
                                                               </div>
                                                      )
                                             })}
                                    </Carousel>
                           </div>
                  </Layout>
         )
}