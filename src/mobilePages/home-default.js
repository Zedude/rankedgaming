import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, MobileLayout, useDoc } from '../components';
import { TimeLine } from '../components/timeline';
import { AupContext } from '../providers/aup';

export const HomeDefaultM = () => {
         const { user } = useContext(AupContext)
         const history = useHistory()
         const [text, setText] = useState('users/nothing')
         useEffect(() => {
                  if (user) {
                           setText(`users/${user.email}`)
                  }
         }, [user])

         return (<>
                  <MobileLayout>
                           {user &&
                                    <TimeLine />
                           }
                           {(!user) &&
                                    <Button className='b-primary c-black' onClick={() => { history.push('/signup') }}>CREATE AN ACCOUNT</Button>
                           }
                  </MobileLayout>
         </>)
}