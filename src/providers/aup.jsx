import React, { createContext, useEffect, useState } from 'react'
import { useFirebase } from '../firebase'

export let AupContext = createContext({
    user: null,
    name: '',
    lastName: '',
    ready: false,
    url: ''
})

export const AupProvider = ({ children }) => {
    let [state, setState] = useState({
        user: null,
        ready: false,
        url: ''
    })
    let { auth } = useFirebase();
    console.log(auth)
    
    useEffect(() => {
        if (!auth) {
            return;
        }
        const subscribe = auth.onAuthStateChanged((authUser) => {
            authUser ? setState({ ready: true, user: authUser }) : setState({ ready: false, user: null });
        })
        return () => { subscribe() }
    }, [auth])

    return (
        <AupContext.Provider value={{ ...state }}>
            {children}
        </AupContext.Provider>
    )
}