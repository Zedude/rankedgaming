import { useFirebase } from '../firebase'
import { useState, useEffect } from 'react';

export const useCollection = (path) => {
    const { firestore } = useFirebase();
    const [data, setData] = useState([]);

    useEffect(() => {
        if (firestore && path.split('/').length % 2) {
            const unsubscribe = firestore.collection(path).onSnapshot((s) => {
                setData(s.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
            })

            return () => unsubscribe();
        }
    }, [firestore, path]);


    const createRecord = (id, data) => {
        firestore.collection(path).doc(id).set({ ...data });
    }

    const updateRecord = (id, data) => {
        firestore.collection(path).doc(id).update({ ...data });
    }

    return { data, createRecord, updateRecord }
}

export const useDoc = (path) => {
    const [data, setData] = useState([]);
    const { firestore } = useFirebase();

    useEffect(() => {
        if (firestore) {
            firestore.doc(path).get().then((doc) => {
                if (doc.exists) {
                    setData(doc.data())
                } else {
                    return {}
                }

            })
        }
    }, [firestore, path]);

    return { data }
}

export const useRTDB = (path) => {
    const [state, setState] = useState('')
    const { rtdb } = useFirebase();

    const addData = (id, data) => {
        rtdb.ref(path + '/' + id).set(data)
    }

    useEffect(() => {
        if (rtdb) {
            const dataRead = rtdb.ref(path);

            dataRead.on('value', function (snapshot) {
                setState(snapshot.val())
            });
        }
    }, [rtdb]);

    return { addData, state }
}

