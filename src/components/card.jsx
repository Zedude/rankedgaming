import React from 'react'

export const Card = (props) => {
         const { title, link, bio, children } = props

         return (
                  <div className="card black c-primary">
                           <div className="card-image waves-effect">
                                    <img className="activator w100" src={link} />
                           </div>
                           <div className="card-content">
                                    <span className="card-title activator">{title}</span>
                                    <p>{bio}</p>
                           </div>
                           <div className="card-reveal black">
                                    <span className="card-title">{title}</span>
                                    <p>{children}</p>
                           </div>
                  </div>
         )
}