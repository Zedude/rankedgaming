import firebase from 'firebase'
import { useState, useEffect } from 'react';
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyAFUZhRRaU9ooGih1Iw-WopS-ziYthyy5E",
    authDomain: "genegaming-69438.firebaseapp.com",
    databaseURL: "https://genegaming-69438.firebaseio.com",
    projectId: "genegaming-69438",
    storageBucket: "genegaming-69438.appspot.com",
    messagingSenderId: "692747912745",
    appId: "1:692747912745:web:9b8bd5328bea5a81757e38",
    measurementId: "G-GVXXWRJKHF"
};

export const useFirebase = () => {
    const [state, setState] = useState({ firebase })

    useEffect(() => {
        let app;
        if (!firebase.apps.length) {
            app = firebase.initializeApp(config);
        }
        let rtdb = firebase.database(app);
        let auth = firebase.auth(app)
        let firestore = firebase.firestore(app);
        setState({ firebase, app, auth, firestore, rtdb });
    }, [config]);

    return state;
}