import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Input, MobileLayout, useCollection, useKeyPress } from '../components';
import { useFirebase } from '../firebase';
import { AupContext } from '../providers/aup';

export const SignUpM = () => {
         const { createRecord } = useCollection('users')
         const history = useHistory();
         const { auth } = useFirebase();
         const { user } = useContext(AupContext);
         const [email, setEmail] = useState('')
         const [password, setPassword] = useState('')
         const [password2, setPassword2] = useState('')
         const [firstName, setFirstName] = useState('')
         const [name, setName] = useState('')
         const [error, setError] = useState('')

         const signUp = async () => {
                  if (!email || !password || !password2) {
                           setError('Please enter the required fields')
                           return;
                  }
                  if (password2 !== password) {
                           setError('Passwords do not match')
                           return;
                  }
                  if (password.toLowerCase() === 'password') {
                           setError('Password is not a good password')
                           return;
                  }
                  try {
                           await auth.createUserWithEmailAndPassword(email, password);
                           createRecord(email, {
                                    lastName: name,
                                    firstName: firstName,
                                    rank: 'Guest'
                           })
                           history.push('/')
                  } catch (e) {
                           setError(e.message);
                  }
         }

         if (user) {
                  history.push('/')
         }

         if (useKeyPress(13)) {
                  signUp()
         }

         return (
                  <MobileLayout>
                           <div className='signUpPageM c-default flex-center'>
                                    <Input type='email' className='w500M' placeholder='E-mail' value={email} onChange={(e) => { setEmail(e.target.value) }}></Input>
                                    <Input className='w500M' placeholder='First Name' value={firstName} onChange={(e) => { setFirstName(e.target.value) }}></Input>
                                    <Input className='w500M' placeholder='Last Name' value={name} onChange={(e) => { setName(e.target.value) }}></Input>
                                    <Input type='password' className='w500M' placeholder='Password' value={password} onChange={(e) => { setPassword(e.target.value) }}></Input>
                                    <Input type='password' className='w500M' placeholder='Confirm Password' value={password2} onChange={(e) => { setPassword2(e.target.value) }}></Input>
                                    <Button className='signUpButton c-black w450M' onClick={signUp}>Sign Up</Button>
                                    <a href='' onClick={() => { history.push('/login') }}>Already have an account?</a>
                                    <div className='c-red'>{error}</div>
                           </div>
                  </MobileLayout>
         )
}